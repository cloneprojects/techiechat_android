package com.zybertron.zoechat.baseUtils;

public class ChatUnread {
    public ChatUnread(String count) {
        this.count = count;
    }

    public String getCount() {

        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    String count;
} 